resource "azurerm_kubernetes_cluster" "complete_example" {
  name = "time_management_example"
  location = var.location
  resource_group_name = var.resource_group_name
  dns_prefix = "complete-example-test-application"

  service_principal {
    client_id = var.client_id
    client_secret = var.client_secret
  }

  default_node_pool {
    name = "aksworkers"
    node_count = 1
    vm_size = "Standard_B2s"
    os_disk_size_gb = 30

  }

  linux_profile {
    admin_username = "adminuser"
    ssh_key {
      key_data = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDKHRzr355JcF/bD38CQ44O3xd+psx1nyxOoq7ba1Q5PI2N3tV4xV8i7f7fCsFyIa6/5yKsQJnL10AbT6KyEZmf+dwBqcuz86NA/AgOrUrUzPQz8CZVTnbGXr8TChZRHt6UpqCHdBc22rm5dDS9HHJ/ONUOnQ8puWHJxCLqyr6M1SjLZ2WY0T5p4fre4Kei419vI3hoK29ULSdo7LVU7diaipWzCL6Lgk3XtbZph6EAD8Kr9g5xXTuoDt4p0wZ+FSTtZ2piAFZGxWOgpkhWCp2F16JUKD3xAFQGA7HR72K0+OXnh0V8s3X5V2yVu/53bYs7pLsk4fGkRZn9aaKNm5p7 johndore@johndore"
    }
  }

  tags = {
    Environment = "Dev"
  }
}

