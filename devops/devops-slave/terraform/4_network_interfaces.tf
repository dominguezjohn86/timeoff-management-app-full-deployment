#Terraform v0.12.4

resource "azurerm_public_ip" "azure_vm_publicip" {
  name = "slave_publicip"
  location = var.location
  resource_group_name = var.resource_group_name
  domain_name_label = "jenkinsnode"
  allocation_method = "Dynamic"
  depends_on = [
    azurerm_resource_group.azure-vm-rg]

}

resource "azurerm_network_interface" "azure_vm_interface" {
  name = "slave_interface"
  location = var.location
  resource_group_name = var.resource_group_name

  ip_configuration {
    name = "azure_vm_ip"
    subnet_id = azurerm_subnet.azure_vm_subnet.id
    private_ip_address_allocation = "dynamic"
    public_ip_address_id = azurerm_public_ip.azure_vm_publicip.id
  }


}
