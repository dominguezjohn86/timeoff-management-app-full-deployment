#Terraform v0.12.4

resource "azurerm_virtual_machine" "azure_vm" {
  name = "slave_node"
  location = var.location
  resource_group_name = var.resource_group_name
  network_interface_ids = [
    azurerm_network_interface.azure_vm_interface.id]
  vm_size = "Standard_B2s"

  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "canonical"
    offer = "UbuntuServer"
    sku = "18.04-LTS"
    version = "latest"
  }

  storage_os_disk {
    name = "slave_disk"
    caching = "ReadWrite"
    create_option = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name = "slave-node"
    admin_username = var.vm_username
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path = "/home/${var.vm_username}/.ssh/authorized_keys"
      key_data = var.admin_ssh_publickey
    }
  }
}
