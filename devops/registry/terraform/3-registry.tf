resource "azurerm_container_registry" "java_complete_example" {
  name = "acrmanagementapp"
  resource_group_name = var.resource_group_name
  location = var.location
  sku = "Standard"
}
