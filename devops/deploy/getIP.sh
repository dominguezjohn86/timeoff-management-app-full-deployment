#!/bin/bash

tag=$1

external_ip=""

while [ -z $external_ip ];
do
    sleep 10
    external_ip=$(kubectl get svc $tag --template="{{range.status.loadBalancer.ingress}}{{.ip}}{{end}}")
    echo $(kubectl get svc)
    echo "To Acces to the management application use this information $external_ip:3000"

done
