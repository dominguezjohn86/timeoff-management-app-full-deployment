pipeline {

    agent none

    stages {

        stage('gitlab') {
            steps {
                echo 'Notify GitLab'
                updateGitlabCommitStatus name: 'build', state: 'pending'
                updateGitlabCommitStatus name: 'build', state: 'success'
            }
        }

        stage('Setting Variables to Build Infrastructure') {
            agent { label 'devops-tools' }
            steps {
                checkout([
                        $class                           : 'GitSCM',
                        branches                         : [[name: "master"]],
                        extensions                       : [[$class: 'CheckoutOption', timeout: 100], [$class: 'CloneOption', timeout: 100]],
                        userRemoteConfigs                : [[credentialsId: 'git-credentials', url: 'https://gitlab.com/dominguezjohn86/terraform-variables.git']],
                        doGenerateSubmoduleConfigurations: false
                ])
                stash includes: 'aks/*', name: 'aks-variables'
                stash includes: 'devops-slave/*', name: 'slave-variables'
                stash includes: 'registry/*', name: 'registry-variables'
            }
        }

        stage('Verify Changes in Jenkins node Infrastructure') {
            agent { label 'devops-tools' }
            steps {
                dir("devops/devops-slave/terraform") {
                    unstash 'slave-variables'
                    sh """
                        mv devops-slave/* .
                        terraform init
                        terraform apply -auto-approve
                       """
                }
            }
        }

        stage('Installing Tools on Jenkins Slave') {
            agent { label 'master' }
            steps {
                checkout scm
                dir("devops/devops-slave/ansible") {
                    sh "ansible-playbook master.yml"
                }
            }
        }

        stage('Unit Test') {
            agent { label 'devops-tools' }
            steps {
                checkout scm
                sh "echo Making Unit Testing "
            }
        }

        stage('Create Azure Container Registry') {
            agent { label 'devops-tools' }
            steps {
                dir("devops/registry/terraform") {
                    unstash 'registry-variables'
                    sh """
                      mv registry/* .
                      terraform init
                      terraform apply -auto-approve
                    """
                }
            }
        }

        stage('Build Docker Image') {
            agent { label 'devops-tools' }
            steps {
                script {
                    sh "docker build --no-cache -t acrmanagementapp.azurecr.io/time_management-app:${env.BUILD_NUMBER} ."
                }
            }
        }

        stage('Push Docker Images to Azure Contain Registry') {
            agent { label 'devops-tools' }
            steps {
                script {
                    withCredentials([azureServicePrincipal('azure-credentials')]) {
                        sh 'az login --service-principal -u $AZURE_CLIENT_ID -p $AZURE_CLIENT_SECRET -t $AZURE_TENANT_ID'
                        sh 'az acr login --name acrmanagementapp'
                        sh "docker push acrmanagementapp.azurecr.io/time_management-app:${env.BUILD_NUMBER} "
                        sh "docker push acrmanagementapp.azurecr.io/time_management-app:latest"
                    }
                }
            }
        }

        stage('Create Azure Kubernetes Service Infrastructure') {
            agent { label 'devops-tools' }
            steps {
                dir("devops/aks/terraform") {
                    unstash 'aks-variables'
                    sh """
                        mv aks/* .
                        terraform init
                        terraform apply -auto-approve  
                       """
                }
            }
        }

        stage('Deploy Application Containers') {
            agent { label 'devops-tools' }
            steps {
                dir('devops/deploy') {
                    withCredentials([azureServicePrincipal('azure-credentials')]) {
                        sh """
                    az login --service-principal -u $AZURE_CLIENT_ID -p $AZURE_CLIENT_SECRET -t $AZURE_TENANT_ID
                    az aks get-credentials --resource-group deployment-test-rg --name time_management_example --overwrite-existing
                    cat deploy-app.yml
                    kubectl apply -f deploy-app.yml 
                    kubectl set image deployment/time-management-deployment time-management=acrmanagementapp.azurecr.io/time_management-app:${env.BUILD_NUMBER}
                    """
                    }
                }
            }
        }

        stage('Get Public IP Assigned To App') {
            agent { label 'devops-tools' }
            steps {
                dir('devops/deploy') {
                    withCredentials([azureServicePrincipal('azure-credentials')]) {
                        sh """
                    az login --service-principal -u $AZURE_CLIENT_ID -p $AZURE_CLIENT_SECRET -t $AZURE_TENANT_ID
                    az aks get-credentials --resource-group deployment-test-rg --name time_management_example --overwrite-existing
                    ./getIP.sh   time-management-service
                    """
                    }
                }
            }
        }

        stage('Delete Infrastructure?') {
            agent { label 'devops-tools' }
            steps {
                script {
                    timeout(120) {
                        input message: 'Approve deployment to Production?', submitter: 'it-ops'
                        infrastrutureDirectories = ['devops/aks/terraform']
                        infrastrutureDirectories.each {
                            dir("${it}") {
                                sh 'terraform init'
                                sh 'terraform destroy -auto-approve'
                            }
                        }
                    }
                    echo "Destroyed!"
                }
            }
        }
    }
}